import os

from bokeh.io import curdoc
from bokeh.models import Div, ColumnDataSource

from bokeh.layouts import layout
from bokeh.plotting import figure

import cbsodata
import pandas as pd

import pdb

desc = Div(text=open(os.path.join(os.path.dirname(__file__), "bapp.html")).read(), sizing_mode="stretch_width")

# source = ColumnDataSource(data=dict(
#     x=[],
#     y=[],
#     color=[],
#     title=[],
#     alpha=[]
# ))

table_identifier = '84669NED'
df = pd.DataFrame(cbsodata.get_data(table_identifier))

data = {
    'x': [1,2,3],
    'y': [0,5,1],
    'color': ["orange", "grey", "orange"],
    'title': ["a", "b", "c"],
    'alpha': [.9, .2, .9]
}

source = ColumnDataSource(data=data)

TOOLTIPS = [("Title", "@title")]

p = figure(
    plot_height=400,
    plot_width=400,
    title="my title",
    toolbar_location=None,
    tooltips=TOOLTIPS,
    sizing_mode="scale_both"
)
p.circle(
    x="x",
    y="y",
    source=source,
    size=7,
    color="color",
    line_color=None,
    fill_alpha="alpha"
)

print(p.id)

def update():
    x_name = 'x'
    y_name = 'y'

    p.xaxis.axis_label = x_name
    p.yaxis.axis_label = y_name
    p.title.text = "plot titel"

    source.data = dict(
        x = [1,2,3],
        y = [0,5,1],
        color = ["orange", "grey", "orange"],
        title = ["a", "b", "c"],
        alpha = [.9, .2, .9]
    )

l = layout([
    [desc],
    [p]
], sizing_mode="scale_both")

update()

curdoc().add_root(l)
curdoc().title = "Supertitel"