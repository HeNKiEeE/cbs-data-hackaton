import os

from bokeh.io import curdoc
from bokeh.models import Div, ColumnDataSource, Range1d, Select, MultiSelect

from bokeh.layouts import layout, column
from bokeh.plotting import figure

import cbsodata
import pandas as pd

from local_store import load_dataFrame



table_identifier = '84669NED'
df = load_dataFrame(table_identifier, 'bapp.h5')
print(df.shape)

with open(os.path.join(os.path.dirname(__file__), "bapp.html")) as f:
    desc = Div(text=f.read(), sizing_mode="stretch_width")

selector_bedrijfsgrootte = MultiSelect(
    title="Bedrijfsgrootte",
    value=["Totaal werkzame personen"],
    options=df['Bedrijfsgrootte'].unique().tolist()
)

selector_bedrijfstakken = MultiSelect(
    title="Bedrijfstakken",
    value=['A-U Alle economische activiteiten'],
    options=df["BedrijfstakkenBranchesSBI2008"].unique().tolist()
)

selector_internat = MultiSelect(
    title="Internationaliserings kenmerken",
    value=["Totaal "],
    options=df["InternationaliseringskenmerkenBedrijf"].unique().tolist()
)

selector_kenmerken = MultiSelect(
    title="Kenmerken Baan en Werknemer",
    value=["Totaal"],
    options=df["KenmerkenBaanEnWerknemer"].unique().tolist()
)

source = ColumnDataSource(data=dict(
    x=[],
    banen=[],
    arbeidsvolume=[]
))

p = figure(
    plot_height=200,
    title="plottitle",
    toolbar_location=None,
    sizing_mode='scale_both'
)

p.y_range = Range1d(0,9000)

p.line(x="x", y="banen", source=source, color="red")
p.line(x="x", y="arbeidsvolume", source=source, color="yellow")

def select_data():
    val_bedrijfsgrootte = selector_bedrijfsgrootte.value
    print(val_bedrijfsgrootte)
    val_bedrijfstakken = selector_bedrijfstakken.value
    val_internat = selector_internat.value
    val_kenmerken = selector_kenmerken.value

    selected = df[df['Bedrijfsgrootte'].isin(val_bedrijfsgrootte)]
    selected = selected[selected['BedrijfstakkenBranchesSBI2008'].isin(val_bedrijfstakken)]
    selected = selected[selected['InternationaliseringskenmerkenBedrijf'].isin(val_internat)]
    selected = selected[selected['KenmerkenBaanEnWerknemer'].isin(val_kenmerken)]

    return selected

def update():
    udf = select_data()
    print(udf.shape)

    source.data = dict(
        x=udf["Perioden"],
        banen=udf["Banen_1"],
        arbeidsvolume=udf["Arbeidsvolume_2"]
    )

controls = [selector_bedrijfsgrootte, selector_bedrijfstakken, selector_internat, selector_kenmerken]
for control in controls:
    control.on_change('value', lambda attr, old, new: update())

inputs = column(*controls, width=320, height=1000)
inputs.sizing_mode= "fixed"

l = layout([
    [desc],
    [inputs, p]
], sizing_mode="scale_both")

update()

curdoc().add_root(l)
curdoc().title = "123"