import pandas as pd
import cbsodata


def load_dataFrame(table: str, storeFile: str = ''):
    """
    Loads DataFrame from either server or local store
    If store is present, dataFrames are stored and loaded
    from the local store when possible.
    Currently there are no protections for large stores.
    Only root folder stores are possible
    """

    store = pd.HDFStore(storeFile)

    try:
        data = store.get(table)
        store.close()
        return data
    except KeyError:
        pass

    data = pd.DataFrame(cbsodata.get_data(table))
    store.put(table, data)  # save it
    store.close()

    return data


def main():
    # small:
    # print(load_dataFrame('82811NED', 'local_store.h5'))

    # large:
    print(load_dataFrame('84669NED', 'local_store1.h5'))


if __name__ == "__main__":
    main()




